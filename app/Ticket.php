<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['total','storeName'];
    protected $hidden = ['id','idUserFK','idImageFK'];
    protected $table = 'ticket';
}
