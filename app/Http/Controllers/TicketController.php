<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function index(){
        if(Auth::user()){
            $tickets = Ticket::join('imageTicket','imageTicket.id','=','ticket.idImageFK')
                ->where('ticket.idUserFK',Auth::user()->id)
                ->get();
            return view('ticketsAll',compact('tickets'));
        }else{
            $message = "¡Inicia sessión para ver mas!";
            //return  view('auth/login')->with('message', 'Inicia sessión para ver mas');
            return  view('auth/login', compact('message'));
        }
    }

    public function store(Request $request){

        $messages = [
            'nombreTienda'  => 'El nombre de la tienda debe ser mayor a 2 caracteres.',
            'total'         => 'El total debe ser mayor a 1 caracter',
            'imagen'        => 'Es necesario que adjuntes una imagen;'
        ];

        $validator = \Validator::make($request->all(), [
            'nombreTienda'  => 'required|min:2',
            'total'         => 'required|min:1|numeric',
            'imagen'        => 'required'
        ],$messages);
        
        if($validator->fails()){
            return redirect()->back()
                ->with('error', 'Ha ocurrido un error. validators')
                ->withErrors($validator)
                ->withInput();
        }else{
        
            $image = $request->file('imagen');
            //dd($image);
            $imageName = "defaultTicket.jpg";
            $imageName = $image->getClientOriginalName();


            $imgTicket = new \App\ImageTicket;
            $imgTicket->urlImageTicket = "/tickets/".$imageName;
            $imgTicket->created_at     = date('Y-m-d H:i:s');
            $saved = $imgTicket->save();
            //dd($imgTicket);

            if ($saved == true) {
                $image->move(public_path('tickets'), $imageName);
            }else{
                $imgTicket->delete();
                return redirect()->back()->with('error', '¡Ha ocurrido un error al guardar la información, por favor intentalo nuevamente!');
            }

            $ticket = new Ticket;
            $ticket->storeName  = $request->nombreTienda;
            $ticket->total      = $request->total;
            $ticket->idUserFK   = Auth::user()->id;

            if ($saved == true){
                $ticket->idImageFK = $imgTicket->id;
            }

            $ban = $ticket->save();
            //dd($ban);

            if ($ban == true || $ban== 1) {
            return redirect()->back()->with('success', 'La información ha sido guardada');
            }else if ($bandera == false){
                return redirect()->back()->with('error', '¡Ha ocurrido un error al guardar la información, por favor intentalo nuevamente!');
            }

        }
    }

    /*public function view($id,$name,$total,$date,$url){

    }*/
    public function view($id){
        $data = Ticket::find($id);
        return view('ticketView', compact('data'));
    }
}
