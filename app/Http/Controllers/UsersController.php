<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    //
    public function login(Request $request){
        $messages = [
            'userName.min'  => 'El Usuario debe ser minimo de 3 caracteres',
            'userName'      => 'Nombre de usuario es necario.',
            'password.min'  => 'La Contraseña debe ser minimo de 6 caracteres',
            'password'      => 'La Contraseña es requerida.',

        ];
        $validator = \Validator::make($request->all(), [
            'userName'     => 'required|min:3|',
            'password'     => 'required|min:6',
        ],$messages);

        if($validator->fails()){
            return redirect()->back()
                ->with('error') //->with('error', 'Ha ocurrido un error. validators')
                ->withErrors($validator)
                ->withInput();
        }else{

            $credentials = $request->only('userName', 'password');

            if(Auth::attempt($credentials)){
                //return "Tu sesion ha iniciado correctamente";
                return redirect()->route('home');
            }else{
                return redirect()->back()
                    ->with('error')
                    ->withErrors(['userName'=>'Usuario o contraseña incorrectos'])
                    ->withInput();
            }
        }
        
    }

    public function store(Request $request){
        $messages = [
            'userName.min'           => 'El Usuario debe ser al menos de 3 caracteres.',
            'userName.unique'        => 'Este Usuario ya existe, ingresa otro.',
            'password'            => 'La Contraseña debe ser al menos de 6 caracteres.',
        ];
        $validator = \Validator::make($request->all(), [
            'userName'               => 'required|min:3|unique:users',
            'password'            => 'required|min:6',
            'ConfirmarContraseña'   => 'required'
        ],$messages);
        
        if($validator->fails()){
            return redirect()->back()
                ->with('error') //->with('error', 'Ha ocurrido un error. validators')
                ->withErrors($validator)
                ->withInput();
        }else{
            
            if($request->password === $request->ConfirmarContraseña){
                $user = new \App\User;

                if($request->Nombre){
                    $user->name= $request->Nombre;
                }
                if($request->Correo){
                    $user->email= $request->Correo;
                }
                $user->userName= $request->userName;
                $user->password= bcrypt($request->password);
                
                $ban = $user->save();

                if ($ban === true || $ban=== 1) {
                    return redirect()->back()->with('success', 'La información ha sido guardada');
                }else if ($bandera == false){
                    return redirect()->back()->with('error', '¡Ha ocurrido un error al guardar la información, por favor intentalo nuevamente!');
                }

            }else{
                return redirect()->back()->with('error', 'No coinciden las contraseñas, por favor intentalo nuevamente');
            }

        }
    }
}
