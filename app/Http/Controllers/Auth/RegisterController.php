<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    public function store(Request $request){
        $messages = [
            'userName.min'           => 'El Usuario debe ser mayor a 3 caracteres.',
            'userName.unique'        => 'Este Usuario ya existe, ingresa otro.',
            'password'            => 'La Contraseña debe ser mayor a 5 caracteres.',
            'ConfirmarContraseña'   => 'La Confirmacion de contraseña debe ser mayor a 5 caracteres.',
        ];
        $validator = \Validator::make($request->all(), [
            'userName'               => 'required|min:3|unique:users',
            'password'            => 'required|min:5',
            'ConfirmarContraseña'   => 'required|min:5'
        ],$messages);
        
        if($validator->fails()){
            return redirect()->back()
                ->with('error') //->with('error', 'Ha ocurrido un error. validators')
                ->withErrors($validator)
                ->withInput();
        }else{
            
            if($request->password === $request->ConfirmarContraseña){
                $user = new \App\User;

                if($request->Nombre){
                    $user->name= $request->Nombre;
                }
                if($request->Correo){
                    $user->email= $request->Correo;
                }
                $user->userName= $request->userName;
                $user->password= $request->password;
                
                $ban = $user->save();

                if ($ban === true || $ban=== 1) {
                    return redirect()->back()->with('success', 'La información ha sido guardada');
                }else if ($bandera == false){
                    return redirect()->back()->with('error', '¡Ha ocurrido un error al guardar la información, por favor intentalo nuevamente!');
                }

            }else{
                return redirect()->back()->with('error', 'No coinciden las contraseñas, por favor intentalo nuevamente');
            }

        }
    }

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

}
