<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageTicket extends Model
{
    protected $fillable = ['urlImageTicket'];
    protected $hidden = ['id'];
    protected $table = 'imageTicket';
}
