<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'Yendy',
            'userName'  => 'Fer123',
            'email'     => 'user@hotmail.com',
            'password'  => bcrypt('pass123'),
            //'api_token' => str_random(50)
        ]);
    }
}
