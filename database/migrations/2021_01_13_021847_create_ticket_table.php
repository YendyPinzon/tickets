<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->string('total');
            $table->string('storeName');
            $table->integer('idUserFK')->nullable()->unsigned();
            $table->foreign('idUserFK')->references('id')->on('users')->onDelete('set null');
            $table->integer('idImageFK')->nullable()->unsigned();
            $table->foreign('idImageFK')->references('id')->on('imageTicket')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
