<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 
Route::get('/home', 'HomeController@index')->name('home');


//Ticket
Route::get('/ticket/add', function(){
    return view('ticketAdd');
});
Route::post('/ticket/store', 'TicketController@store');
Route::get('/ticket/all', 'TicketController@index');
//Route::get('/ticket/{id}/{name}/{total}/{date}/{url}', 'TicketController@view');
Route::get('/ticket/view/{id}', 'TicketController@view');
/*Route::get('/ticket/view/', function($row){
    return view('ticketView')->with($row);
});*/



//Users
Route::post('/register/save','UsersController@store');
//Route::post('/register/save','Auth\RegisterController@store'); PASAR Y HACER PRUEBAS
//Route::post('/login/verify','Auth\LoginController@login');
Route::post('/login/verify','UsersController@login');




Auth::routes();

/*Route::group(['middleware' => ['auth:api']], function () {
    Route::get('test', function () {
        $user = \Auth::user();
        return $user;
    });
});*/
