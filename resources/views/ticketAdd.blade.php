@extends('layouts.app')
@section('content')

@if (\Session::has('success'))
    <script type="text/javascript">
        swal("¡Éxito!", "{{ Session::get('success') }}", "success");
    </script>
@elseif (\Session::has('error'))
    <script type="text/javascript">
        //swal("¡ÉRROR!", "{{ Session::get('error') }}", "error");
        Swal.fire({
            title: '<strong>ERROR</strong>',
            type: 'error',
            html:$(".errorGett").html(),
            showCloseButton: false,
            showCancelButton: false,
        });
    </script>
@endif

@if(auth::user())
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="text-secondary">Añadir ticket</h1>
        </div>
        <div class="col-6 text-right">
            <a href="{{ url()->previous() }}" class="btn btn-info btn-rounded bg-prim-color"><i class="fas fa-angle-double-left"></i> Atras</a>
        </div>
    </div>

    <form method='POST' action="{{url('/ticket/store')}}" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <div class="form-group">
            <label class="form-label" for="nombreTienda">Nombre de la tienda</label>
            <input type="text" id="nombreTienda" name="nombreTienda" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="form-label" for="total">Total de la compra</label>
            <div class="input-group mb-3">
                <span class="input-group-text"  id="basic-addon1">$</span>
                <input type="text" id="total" name="total" class="form-control" required aria-describedby="basic-addon1"/>
            </div>
        </div>
        <div class="custom-file">
            <label class="custom-file-label" for="archivo">Imagen del ticket</label>
            <input type="file" id="archivo" name="imagen" class="imagen custom-file-input" accept=".png, .jpg, .jpeg, .JPG, .PNG, .JPEG" required/>
        </div> <br> <br>
        <button class="btn btn-outline-secondary" type="submit">Añadir</button>

    </form>
</div>
@else
    @include('auth/login', ['message' => "¡Inicia sessión para ver mas!"])
@endif

@endsection