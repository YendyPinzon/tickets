@extends('layouts.app')
@section('content')

@if(auth::user())
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="text-info">Mis tickets</h1>
        </div>
        <div class="col-6 text-right">
            <a href="{{ url()->previous() }}" class="btn btn-info btn-rounded bg-prim-color"><i class="fas fa-angle-double-left"></i> Atras</a>
        </div>
    </div>
    <br>

        @if(count($tickets) > 0)
        
            <div class=""> <!--card-group-->
                <div class=" row  row-cols-1 row-cols-md-3 g-4 ">
              
                @foreach($tickets as $row)
                    @php
                        $enalceTicket = url('').$row->urlImageTicket;
                    @endphp

                    <div class="col  card-group">
                        <div class="card  h-100">
                            @if($row->urlImageTicket)
                                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                    <img src="{{$enalceTicket}}" alt="Ticket" class=" img-fluid ">
                                    <a href="#"> <!-- {{url('/ticket/view/'.$row->id)}} -->
                                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                                    </a>
                                </div>
                            @endif

                            <div class="card-body">
                            </div>

                            <div class="card-footer text-center">
                                <h5 class="card-title">Tienda <br> {{$row->storeName}}</h5>
                                <p class="card-text">
                                     Total: ${{$row->total}} <br> {{\Carbon\Carbon::parse($row->created_at)->format('d/m/Y') }}
                                </p>
                                <!--<a href="#!" class="btn btn-primary">Button</a>-->
                            </div>
                        </div>

                    </div>

                   
            
                @endforeach
                </div>
            </div>

            
            
        @else
            <br>
            <h3>No has añadido ningun ticket</h3>
        @endif
    </div>

</div>


@else

@include('guest')

@endif

@endSection