@extends('layouts.app')
@section('content')

@if(auth::user())
<div class="container">
    
    <div>
        {{$data}}
        @php
            $image = url('').$data->urlImageTicket;
        @endphp
        <img src="{{$image}}" alt="Ticket" class=" img-fluid ">
    </div>
</div>

@else
    @include('guest')
@endif

@endSection