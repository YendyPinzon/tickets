@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">   
                    <h4 class=" ">Mis datos</h4> 
                </div>

                <div class="card-body">
                    <div class="text-center">
                        
                        <div class="form-group row">
                            <label for="name" class=" col-md-3 col-form-label ">Nombre</label>
                            <div class="col-md-9">
                                <input type="email" name="name" class="form-control" value="{{ auth()->user()->name}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class=" col-md-3 col-form-label ">Correo</label>
                            <div class="col-md-9">
                                <input type="email" name="email" class="form-control" value="{{ auth()->user()->email}}" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="userName" class=" col-md-3 col-form-label ">Usuario</label>
                            <div class="col-md-9">
                                <input type="email" name="userName" class="form-control" value="{{ auth()->user()->userName}}" disabled>
                            </div>
                        </div>

                    </div>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
