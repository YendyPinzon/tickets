@extends('layouts.app')

@section('content')


@if (\Session::has('success'))
    <!--{{ Session::get('success') }}-->
    <script type="text/javascript">
        Swal.fire({
            position: 'top-center',
            type: 'success',
            title: "{{ Session::get('success') }}",
            showConfirmButton: false,
            timer: 1500
        });
        
    </script>
@endif

@if(!empty($message) === true)
    <div class="text-center">
        <h1>{{$message}}</h1>
    </div>
@endif
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card no-shadow">
                <div class="card-header text-warning text-uppercase ">{{ __('Iniciar sesión') }}</div>

                <div class="card-body">
                <form method="POST" action="{{ url('/login/verify') }}" enctype="multipart/form-data">
                    <!--{{ route('register') }}
                        <!--@csrf-->
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="userName" class="col-sm-4 col-form-label text-md-right">{{ __('Nombre de usuario') }}</label>

                            <div class="col-md-6">
                                    <input id="userName" type="text" class="form-control{{ $errors->has('userName') ? ' is-invalid' : '' }}" name="userName" value="{{ old('userName') }}" required autofocus aria-describedby="basic-addon1">
                                
                                @if ($errors->has('userName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('userName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <br>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Mantener sesión') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-warning btn-rounded"> <!--btn-outline-warning -->
                                    {{ __('Ingresar') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
