@extends('layouts.app')
@section('content')

@if (\Session::has('success'))
    <!--{{ Session::get('success') }}-->
    <script type="text/javascript">
        Swal.fire({
            position: 'top-center',
            type: 'success',
            title: "{{ Session::get('success') }}",
            showConfirmButton: false,
            timer: 1500
        });
        
    </script>
@elseif (\Session::has('error'))
    <!--{{ Session::get('error') }}-->
    <script type="text/javascript">
        Swal.fire({
            type: 'error',
            title: 'Error...',
            text: "{{ Session::get('error') }}",
        });
    </script>
@endif


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card no-shadow">
                <div class="card-header text-warning text-uppercase ">{{ __('Datos de registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/register/save') }}" enctype="multipart/form-data">
                    <!--{{ route('register') }}
                        <!--@csrf-->
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="Nombre" value="{{ old('Nombre') }}"  autofocus>

                                <!--@if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif-->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>

                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="Correo" value="{{ old('Correo') }}"  aria-describedby="basic-addon1">

                                </div>

                                <!--@if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif-->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="userName" class="col-md-4 col-form-label text-md-right">{{ __('Usuario*') }}</label>

                            <div class="col-md-6">
                                <input id="userName" type="text" class="form-control{{ $errors->has('userName') ? ' is-invalid' : '' }}" name="userName" value="{{ old('userName') }}" required>
                                @if ($errors->has('userName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('userName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña*') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña*') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="ConfirmarContraseña" value="{{ old('ConfirmarContraseña') }}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-warning btn-rounded ">
                                    {{ __('Registrarme') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
